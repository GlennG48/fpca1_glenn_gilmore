import Data.List
import Data.Maybe

-- extra function needed to define vowels
isvowel 'a' = True
isvowel 'e' = True
isvowel 'i' = True
isvowel 'o' = True
isvowel 'u' = True
isvowel _ = False

-- Question 1
-- this type declaration takes in two numers and returns one number
-- this adds the two numbers and then doubles the answer from adding
add_and_double::(Num a)=>a->a->a
add_and_double a b = (a + b)*2

-- Question 2
-- this function should do the same as the first function
-- this function uses backticks to call the first function
(+*)::(Num a)=>a->a->a
(+*) a b = a `add_and_double` b

-- Question 3
-- this function solves quadratic equations, it takes in 3 doubles and returns a tuple of doubles
-- the where clause splits the equation up to make the equation simplier
-- e gets -b devided by 2 times a and d gets b squared - 4 times a times c divided by 2 times a
-- the equation is then put together in the x1 and x2 lines to give the answer
solve_quadratic_equation::Double->Double->Double->(Double, Double)
solve_quadratic_equation a b c = (x1,x2)
    where
        x1 = e + sqrt d / (2 * a)
        x2 = e - sqrt d / (2 * a)
        d = b * b - 4 * a * c
        e = - b / (2 * a)

-- Question 4
-- this code takes in an integer and a list and returns the first n numbers from the list, n = int
-- it checks to see if the list is empty or the int passed is 0
first_n::Int->[a]->[a]
first_n 0 _ = []
first_n _ [] = []
first_n n (x:xs) = x : first_n( n-1) xs

-- Question 5
-- this function takes in an integer and returns a list of integers, this function calls another function clled take_integer
-- the second function takes the inputted integer ammount from the list that was entered
first_n_integers::Integer->[a]->[a]
first_n_integers = take_integer
    where
        take_integer::Integer->[a]->[a]
        take_integer 0 _ = []
        take_integer _ [] = error"The List is Not long enough"
        take_integer n (x:xs) = x : take_integer( n-1) xs

-- Question 6 Partially working
-- 
double_factorial::Integer->Integer
double_factorial = factorial
    where
        factorial::Integer->Integer
        factorial 0 = 1
        factorial n = n * factorial (n-1)


-- Question 7
-- not working
--factorials::Integer->[Integer]
--factorials x = 1 : zipWith (*) factorials[1..x]


-- Question 8
-- this function takes in an integer and uses a list comprehension to check if the number is prime
-- the list comprehension takes the number that was given and divides it by an increasing number k
-- and then mods the number and checks to see if there is a remainder if not returns false otherwise true
isPrime::Integer->Bool
isPrime k = null [ x | x <- [2..k - 1], k `mod`x  == 0]

-- Question 9
-- this function takes in a integer list and takes out all the prime numbers, this is done by a list comprehension,
-- y is less than the length of the list and this is then modded with x and if there is no remainder it isnt prime
-- if there is it is prime
primes::[Integer]
primes = group [2..]
    where
        group (x:xs) = x : group [y|y <- xs, y `mod` x > 0]

-- Question 10
-- this function uses recursion to add up the integers in a list
sum_list_of_integers::[Integer]->Integer
sum_list_of_integers [] = 0
sum_list_of_integers (x:xs) = x + sum_list_of_integers xs

-- Question 11
-- this function takes in a integer list and sums up the list using the foldl function, this starts counting from the right and moves left
sum_using_foldl::[Integer]->Integer
sum_using_foldl xs = foldl(\ acc x -> acc + x)0 xs

-- Question 12
-- this function uses foldr to get the product of the list that is supplied to the function
product_using_foldr::[Integer]->Integer
product_using_foldr = foldr (*) 1

-- Question 13
--guess::String->Int->String
--guess "i love functional programming" parseInt(x<5) = "you have won"
--guess _ parseInt(x<5) = "guess again"
--guess _ parseInt(x>=5) = "you have lost"


-- Question 14
-- this function takes in two integer lists and returns the dot product of the two lists
-- it does this by taking the head out of both lists and multiplies them together and 
-- uses recursion to get all elements out of the lists
find_dot_product_of_vectors::[Integer]->[Integer]->Integer
find_dot_product_of_vectors [] [] = 0
find_dot_product_of_vectors (x:xs) (y:ys) = x * y + find_dot_product_of_vectors xs ys

-- Question 15
-- this function takes in an integer and checks to see if it is even, the function does this by
-- modding the input by 2 to see if there is a remainder, if the remainder is 0 it will return true
-- otherwise the function will return false
is_even::Integer->Bool
is_even n
    |n `mod` 2 == 0 = True
    |otherwise = False

-- Question 16 
-- this function takes in a char list, the function filters through the list and takes out an element 
-- that is aeiouAeiou and leaves the rest of the elements in a char list
unixname::[Char]->[Char]
unixname = filter(`notElem` "aeiouAEIOU")

-- Question 17
-- this function takes in two lists and checks to see if any elements in the two lists are the same,
-- if an element is present in both lists it is put into a list that the function returns
intersection::(Eq a)=>[a]->[a]->[a]
intersection xs ys = xs `intersect` ys

-- Question 18
-- this function takes in a list of chars, it then uses the isvowel function above to check if the value is a vowel
-- if the char is a vowel it is replaced with a x, otherwise it goes onto the next char
censor::[Char]->[Char]
censor [] = []
censor (c:cs) | isvowel c = 'x':(censor cs)
              | otherwise = c:(censor cs)
    
-- Finish